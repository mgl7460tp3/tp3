/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.math.BigInteger;

/**
 *
 * @author takus
 */
public class Customer {

    private String name;
    private double phoneNumber;
    private String address;
    private int customerNumber;
    private String Email;
    private int points;

    public Customer(String name, double phoneNumber, String address/*, int customerNumber*/, String Email, int points) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
      //  this.customerNumber = customerNumber;
        this.Email = Email;
        this.points = points;
    }

    public Customer() {
        this.name = "Taki";
        this.customerNumber = 1;
        this.points = 0;
    }

    public Customer(String name, int customerNumber) {
        this.name = name;
        this.customerNumber = customerNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(double phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public int getPoints() {
        return this.points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void increaseCustomerPoints(int addedPoints) throws ImpossibleValueException {
        if (addedPoints < 0) {
            throw new ImpossibleValueException();
        }
        this.points += addedPoints;
    }

    public void decreaseCustomerPoints(int removedPoints) throws ImpossibleValueException {
        if (removedPoints < 0 || this.points < removedPoints) {
            throw new ImpossibleValueException();
        }
        this.points -= removedPoints;
    }

}
