/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

/**
 *
 * @author kevin
 */
public class Product {
    private float price;
    private String name;
    private String category;
    private int quantity =10;
    boolean available;
    
    public Product(float price){
        this.price = price;
    }

    public Product(String name) {
        this.name = name;
    }
    
    public Product(String name, String category){
        this.name = name;
        this.category = category;
    }
    
    public Product(String name, String category, float price, int quantity){
        this.name = name;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
    }
 
    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public String showProductPrice(){
        String productPrice = String.format("%.2f", this.price) + "$";
        return productPrice;
    }
    
    public String showProductName(){
        String productName = "Le nom de ce produit est " + this.name +".";
        return productName;
    }
    
    public String showProductCategory(){
        String productCategory = "Le produit " + this.name + " appartient à la catégorie " + this.category+".";
        return productCategory;
    }
    
    public String showProductQuantity(){
     String ProductQuantity = "Il y a "+this.quantity+ " unités pour ce produit.";
     return ProductQuantity;
    }    
    
    public void increaseProductQuantity(int quantityAdded) throws ImpossibleValueException{
        if (quantityAdded<0)
        {
            throw new ImpossibleValueException();
        }
        this.quantity += quantityAdded;
    }
    
    public void decreaseProductQuantity(int quantityRemoved) throws ImpossibleValueException{
        if (this.quantity<quantityRemoved || quantityRemoved <0)
        {
            throw new ImpossibleValueException();
        }
        this.quantity -= quantityRemoved;
    }
    
    public boolean isAvailable(){
       return (this.quantity > 0);
    }
}
