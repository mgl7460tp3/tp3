
package tp3;

/**
 *
 * @author takus
 */
import java.util.Scanner;
public class Purchase
        
{
    private String product;
    private int uCount;
    private float uPrice;
                     
    private int numberBought;
 
    public void setName(String newName)
    {
        product = newName;
    }
 
   
 
    public void setPrice(float price)
    {
            uPrice = price;   
    }
 
    public void setNumberBought(int number)
    {
        if (number <= 0)
        {
            System.out.println("Error: Bad parameter in setNumberBought.");
            System.exit(0);
        }
        else
            numberBought = number;
    }
 
    
   
    
    

    public void Output()
    {
        System.out.println(numberBought + " " + product);
        
    }
 
    public String getName()
    {
        return product;
    }
 
    public double getTotalCost()
    {
        return ((uPrice)*numberBought);
    }
 
    public double getUnitCost()
    {
        return (uPrice);
    }
 
    public int getNumberBought()
    {
        return numberBought;
    }
}