package tp3;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
 

public class Shop {

    private ArrayList<Product> ProductList;
    private ArrayList<Customer> CustomerList;
    private final String CUSTOMER_FILE = "Customer.xml";
    private final String PRODUCT_FILE = "Product.xml";

    public Shop() throws SAXException, IOException {
        this.ProductList = new ArrayList<>();
        this.CustomerList = new ArrayList<>();
        getAllCustomerFromXML();
        getAllProductFromXML();
    }

    public int getNumberOfProductsByName() {
        return this.ProductList.size();
    }

    public int getNumberOfProductsBycategory() {
        int count = 0;
        ArrayList<String> checked = new ArrayList<>(Arrays.asList());
        for (int i = 0; i < this.ProductList.size(); i++) {
            String str = this.ProductList.get(i).getCategory().trim().toLowerCase();
            if (!checked.contains(str)) {
                checked.add(str);
                count++;
            }
        }
        return count;
    }

//    public ArrayList<Customer> sortCusomerByPoints()
//    {  
//        ArrayList<Customer> sortedlist ; 
//        
//        return 
//    }
//  
    public int getNumberOfCustomer() {
        return this.CustomerList.size();
    }

    public void getAllCustomerFromXML() {

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            final DocumentBuilder builder = factory.newDocumentBuilder();

            final Document document = builder.parse(new File(CUSTOMER_FILE));

            final Element racine = document.getDocumentElement();

            final NodeList racineNoeuds = racine.getChildNodes();
            final int nbRacineNoeuds = racineNoeuds.getLength();

            for (int i = 0; i < nbRacineNoeuds; i++) {
                if (racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    final Element customer = (Element) racineNoeuds.item(i);

                    final Element customerNumber = (Element) customer.getElementsByTagName("id").item(0);
                    final Element name = (Element) customer.getElementsByTagName("name").item(0);
                    final Element phone = (Element) customer.getElementsByTagName("phoneNumber").item(0);
                    final Element address = (Element) customer.getElementsByTagName("address").item(0);
                    final Element email = (Element) customer.getElementsByTagName("email").item(0);
                    final Element points = (Element) customer.getElementsByTagName("points").item(0);
                    //System.out.println(name.getTextContent().toString() + " "  +  customerNumber.getTextContent()  /*+ " "  + address.getTextContent().toString() + customerNumber.getTextContent().toString() +  email.getTextContent().toString() + points.getTextContent().toString()*/);
                    this.CustomerList.add(new Customer(name.getTextContent().toString(), Double.parseDouble(phone.getTextContent().toString()), address.getTextContent(), email.getTextContent(), Integer.parseInt(points.getTextContent())));
                }
            }
        } catch (final ParserConfigurationException e) {
            e.printStackTrace();
        } catch (final SAXException e) {
            e.printStackTrace();
        } catch (final IOException e) {
            e.printStackTrace();
        }

    }

    public void getAllProductFromXML() throws SAXException, IOException {

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            final DocumentBuilder builder = factory.newDocumentBuilder();

            final Document document = builder.parse(new File(PRODUCT_FILE));
            final Element racine = document.getDocumentElement();

            final NodeList racineNoeuds = racine.getChildNodes();
            final int nbRacineNoeuds = racineNoeuds.getLength();

            for (int i = 0; i < nbRacineNoeuds; i++) {
                if (racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    final Element produit = (Element) racineNoeuds.item(i);

                    final Element name = (Element) produit.getElementsByTagName("name").item(0);
                    final Element category = (Element) produit.getElementsByTagName("category").item(0);
                    final Element price = (Element) produit.getElementsByTagName("price").item(0);
                    final Element quantity = (Element) produit.getElementsByTagName("quantity").item(0);
                    this.ProductList.add(new Product(name.getTextContent(), category.getTextContent(), Float.parseFloat(price.getTextContent()), Integer.parseInt(quantity.getTextContent())));
                }
            }
        } catch (final ParserConfigurationException e) {
            e.printStackTrace();
        } catch (final SAXException e) {
            e.printStackTrace();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    //todo 
    public Product getProductbyname(String name) {

        for (int i = 0; i < ProductList.size(); i++) {
            if (ProductList.get(i).getName().equals(name)) {
                return new Product(ProductList.get(i).getName(), ProductList.get(i).getCategory(), ProductList.get(i).getPrice(), ProductList.get(i).getQuantity());
            }
        }
        return null;
    }

    public Customer getcustomerbyname(String name) {
        
        for (int i = 0; i < CustomerList.size(); i++) {
            
            if (CustomerList.get(i).getName().toLowerCase().equals(name.toLowerCase())) {   
                return new Customer(CustomerList.get(i).getName().toLowerCase(), CustomerList.get(i).getPhoneNumber(), CustomerList.get(i).getAddress() , CustomerList.get(i).getEmail(),  CustomerList.get(i).getPoints());
            }
        }
        return null;
    }
    
    public void setCustomerXML() throws SAXException, IOException
    { 
        XML xmlConstructor = new XML() ;
        xmlConstructor.writeProductXmlFile(this.ProductList); 
    }
    public void displayCustomer() 
    { 
        for(int i = 0 ; i < CustomerList.size();i++)
        { 
           System.out.println(CustomerList.get(i).getName());
        }   
    }
    
        public void displayProduct() 
    { 
        for(int i = 0 ; i < ProductList.size();i++)
        { 
           System.out.println(ProductList.get(i).getName() + " price : " + ProductList.get(i).getPrice());
        }   
    }
    
}
