package tp3;


// Ce code provient de : https://openclassrooms.com/courses/structurez-vos-donnees-avec-xml/dom-exemple-d-utilisation-en-java


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class XML {
    
    public void writeProductXmlFile(ArrayList<Product> list) {
    
    try {

        DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
        DocumentBuilder build = dFact.newDocumentBuilder();
        Document doc = build.newDocument();
        
        Element root = doc.createElement("Products");
        doc.appendChild(root);

        
        
        for (Product produit : list) {
            
            Element Product = doc.createElement("Product");            
            Element name = doc.createElement("name");
            
            name.appendChild(doc.createTextNode(String.valueOf(produit.getName())));
            Product.appendChild(name);
            
            Element category = doc.createElement("category");
            category.appendChild(doc.createTextNode(String.valueOf(produit.getCategory())));
            Product.appendChild(category);
            
            Element price = doc.createElement("price");
            price.appendChild(doc.createTextNode(String.valueOf(produit.getPrice())));
            Product.appendChild(price);
            
            
            Element quantity = doc.createElement("quantity");
            quantity.appendChild(doc.createTextNode(String.valueOf(produit.getQuantity())));
            Product.appendChild(quantity);
            
            root.appendChild(Product);
        }

        // Save the document to the disk file
        TransformerFactory tranFactory = TransformerFactory.newInstance();
        Transformer aTransformer = tranFactory.newTransformer();

        DOMSource source = new DOMSource(doc);
        try {
            // location and name of XML file you can change as per need
            FileWriter fos = new FileWriter("C:\\Users\\hc091085\\Documents\\tp3\\tp3\\TP3\\ros.xml");
            StreamResult result = new StreamResult(fos);
            aTransformer.transform(source, result);
        } catch (IOException e) {

            e.printStackTrace();
        }

    } catch (TransformerException ex) {
        System.out.println("Error outputting document");

    } catch (ParserConfigurationException ex) {
        System.out.println("Error building document");
    }
}
  
    public XML () throws SAXException, IOException{
        /*
	 * Etape 1 : récupération d'une instance de la classe "DocumentBuilderFactory"
	 */

        /*
         * Etape 1 : récupération d'une instance de la classe "DocumentBuilderFactory"
         */
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            	
        try {
            /*
             * Etape 2 : création d'un parseur
             */
            final DocumentBuilder builder = factory.newDocumentBuilder();
			
	    /*
	     * Etape 3 : création d'un Document
	     */
	    final Document document= builder.parse(new File("Product.xml"));
			
	    //Affichage du prologue
	    System.out.println("*************PROLOGUE************");
	    System.out.println("version : " + document.getXmlVersion());
	    System.out.println("encodage : " + document.getXmlEncoding());		
            System.out.println("standalone : " + document.getXmlStandalone());
					
	    /*
	     * Etape 4 : récupération de l'Element racine
	     */
	    final Element racine = document.getDocumentElement();
		
	    //Affichage de l'élément racine
	    System.out.println("\n*************RACINE************");
	    System.out.println(racine.getNodeName());
		
	    /*
	     * Etape 5 : récupération des personnes
	     */
	    final NodeList racineNoeuds = racine.getChildNodes();
	    final int nbRacineNoeuds = racineNoeuds.getLength();
			
	    for (int i = 0; i<nbRacineNoeuds; i++) {
	        if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
	            final Element personne = (Element) racineNoeuds.item(i);
				
		    //Affichage d'une personne
		   
			
	    	    /*
		     * Etape 6 : récupération du nom et du prénom
		     */
		    final Element nom = (Element) personne.getElementsByTagName("name").item(0);
		    final Element prenom = (Element) personne.getElementsByTagName("category").item(0);
					
		    //Affichage du nom et du prénom
		    System.out.println("name : " + nom.getTextContent());
		    System.out.println("category : " + prenom.getTextContent());
					
		    /*
		     * Etape 7 : récupération des numéros de téléphone
		     */
		    final NodeList telephones = personne.getElementsByTagName("telephone");
		    final int nbTelephonesElements = telephones.getLength();
					
		    for(int j = 0; j<nbTelephonesElements; j++) {
		        final Element telephone = (Element) telephones.item(j);
		    
                        //Affichage du téléphone
                        System.out.println(telephone.getAttribute("type") + " : " + telephone.getTextContent());
		    }
	        }				
	    }			
        }
        catch (final ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (final SAXException e) {
            e.printStackTrace();
        }
        catch (final IOException e) {
            e.printStackTrace();
        }		
    }
}
