/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author takus
 */
public class CustomerTest {
    
    @Test
    public void TestIncreaseCustomerPoints() throws ImpossibleValueException{
        Customer customer = new Customer();
        customer.increaseCustomerPoints(9);
        assertEquals(customer.getPoints(),9);
    }
    
    @Test (expected=ImpossibleValueException.class)
    public void TestIncreaseNegativeCustomerPoints() throws ImpossibleValueException{
        Customer customer = new Customer();
        customer.increaseCustomerPoints(-9);
    }
    
    @Test
    public void TestDecreaseCustomerPoints() throws ImpossibleValueException{
        Customer customer = new Customer();
        customer.setPoints(5);
        customer.decreaseCustomerPoints(2);
        assertEquals(customer.getPoints(),3);
    }
    
    @Test (expected=ImpossibleValueException.class)
    public void TestDecreaseNegativeCustomerPoints() throws ImpossibleValueException{
        Customer customer = new Customer();
        customer.setPoints(5);
        customer.decreaseCustomerPoints(-2);
    }
    
     @Test (expected=ImpossibleValueException.class)
    public void TestDecreaseUnderZeroCustomerPoints() throws ImpossibleValueException{
        Customer customer = new Customer();
        customer.setPoints(5);
        customer.decreaseCustomerPoints(6);
    }
}
    
    
    
    

  


    

