/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kevin
 */
public class ProductTest {
    

    @Test
    public void testShowProductPrice() {
        Product product = new Product(3);
        assertEquals(product.showProductPrice(), "3.00$");
    }
    
    @Test
    public void testShowProductName(){
        Product product = new Product("chaise");
        assertEquals(product.showProductName(),"Le nom de ce produit est chaise.");
    }
    
    @Test
    public void testShowProductCategory(){
        Product product = new Product("chaise", "meuble");
        assertEquals(product.showProductCategory(),"Le produit chaise appartient à la catégorie meuble.");
    }
    
    @Test
    public void testShowQuantity(){
        Product product = new Product("chaise", "meubl", 3, 12);
        assertEquals(product.showProductQuantity(),"Il y a 12 unités pour ce produit.");
    }
   
    @Test
    public void testIncreaseProductQuantity() throws ImpossibleValueException{
        Product product = new Product("chaise", "meuble", 3, 12);
        product.increaseProductQuantity(5);
        assertEquals(product.getQuantity(),17);
    }
    
    @Test(expected=ImpossibleValueException.class)
    public void testIncreaseNegativeProductQuantity()throws ImpossibleValueException{
        Product product = new Product("chaise", "meuble", 3, 12);
        product.increaseProductQuantity(-5);
    }
    
    @Test
    public void testDecreaseProductQuantity() throws ImpossibleValueException{
        Product product = new Product("chaise", "meuble", 3, 12);
        product.decreaseProductQuantity(5);
        assertEquals(product.getQuantity(),7);
    }
    
    @Test(expected=ImpossibleValueException.class)
    public void testDecreaseProductQuantityUnderZero() throws ImpossibleValueException{
        Product product = new Product("chaise", "meuble", 3, 1);
        product.decreaseProductQuantity(5);
    }
    
    @Test(expected=ImpossibleValueException.class)
    public void testDecreaseNegativeProductQuantity()throws ImpossibleValueException{
        Product product = new Product("chaise", "meuble", 3, 12);
        product.decreaseProductQuantity(-5);
    }
    
    @Test
    public void testIsAvailable(){
        Product product = new Product("chaise", "meuble", 3, 5);
        assertTrue(product.isAvailable());
    }
    
    @Test
    public void testIsNotAvailable(){
        Product product = new Product("chaise", "meuble", 3, 0);
        assertFalse(product.isAvailable());
    }
}
