/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author takus
 */
public class PurchaseTest {
    
    public PurchaseTest() {
    
    }
    
    @Test
    public void testGetTotalCost() {
        Purchase instance = new Purchase();
        instance.setNumberBought(3);
        instance.setPrice(4);
        double expResult = 12.0;
        double result = instance.getTotalCost();
        assertEquals(expResult, result, 0.01);
    }
 
    @Test
    public void testGetUnitCost() {
        Purchase instance = new Purchase();
        instance.setNumberBought(3);
        instance.setPrice(4);
        double expResult = 4.0;
        double result = instance.getUnitCost();
        assertEquals(expResult, result,0.0);
    }
}
