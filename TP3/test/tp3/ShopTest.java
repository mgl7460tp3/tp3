/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.io.IOException;
import java.util.ArrayList;

import org.xml.sax.SAXException;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ShopTest {

    @Test
    public void getNumberOfProductsByNameTest() {

        Product p1 = new Product("Banana", "Aliment", (float) 1.2, 80);
        Product p2 = new Product("pasta", "Aliment", (float) 1.8, 50);
        Product p3 = new Product("Ipad", "Electronique", (float) 814.0, 10);

        ArrayList<Product> ProductList = new ArrayList<>();
        ProductList.add(p1);
        ProductList.add(p2);
        ProductList.add(p3);
        try {
            Shop shop = new Shop();
            assertEquals(6, shop.getNumberOfProductsByName());
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    @Test
    public void getNumberOfProductsByCategoryTest() {
        Product p1 = new Product("Banana", "aliment", (float) 1.2, 80);
        Product p2 = new Product("pasta", "aliment", (float) 1.8, 50);
        Product p3 = new Product("Ipad", "electronique", (float) 814.0, 10);
        Product p4 = new Product("machine", "Electromenager", (float) 814.0, 10);

        ArrayList<Product> ProductList = new ArrayList<>();
        ProductList.add(p1);
        ProductList.add(p2);
        ProductList.add(p3);
        ProductList.add(p4);
        try {

            Shop shop = new Shop();
            assertEquals(3, shop.getNumberOfProductsBycategory());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void getProductbynameTest() {
        try {
            Shop shop = new Shop();
            shop.getProductbyname("Banane");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void getNumberOfCustomerTest() {

        try {
            Shop shop = new Shop();
            assertEquals(4, shop.getNumberOfCustomer());

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void getcustomerbynameTest() {
        try {
            Shop shop = new Shop();
            assertEquals("dali", shop.getcustomerbyname("dali").getName());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Test
    public void setCustomerXMLTest() {
      try {
            Shop shop = new Shop();
            shop.setCustomerXML();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
